<?php


namespace SmartOSC\Blog\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {

        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {

            $setup->getConnection();
            $setup->getConnection()->addColumn(
                $setup->getTable('smartosc_categories'),
                'parent_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => true,
                    'unsigned' => true,
                    'default' => 0,
                    'size' => null,
                    'comment' => 'Parent Id',

                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('smartosc_categories'),
                'position',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => false,
                    'unsigned' => true,
                    'default' => 0,
                    'size' => null,
                    'comment' => 'Position',

                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('smartosc_categories'),
                'level',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => false,
                    'unsigned' => true,
                    'default' => 0,
                    'size' => null,
                    'comment' => 'Position',

                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('smartosc_categories'),
                'children_count',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => false,
                    'unsigned' => true,
                    'default' => 0,
                    'size' => null,
                    'comment' => 'Children count',

                ]
            );
            $setup->endSetup();
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $setup->getConnection();
            $setup->getConnection()->addColumn(
                $setup->getTable('smartosc_blog'),
                'short_content',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'size' => null,
                    'comment' => 'Short Content',
                ]
            );


        }
    }
}