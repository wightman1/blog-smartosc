<?php


namespace SmartOSC\Blog\Model;


class Blog extends \Magento\Framework\Model\AbstractModel
{



    protected $_eventPrefix = 'blogs';

    protected $_eventObject = 'blog';

    protected $_idFieldName = 'blog_id';


    public function getEnableStatus() {
        return 1;
    }

    public function getDisableStatus() {
        return 0;
    }

    public function getAvailableStatuses() {
        return [$this->getDisableStatus() => __('Disabled'), $this->getEnableStatus() => __('Enabled')];
    }
    protected function _construct()
    {
        $this->_init('SmartOSC\Blog\Model\ResourceModel\Blog');
    }
}