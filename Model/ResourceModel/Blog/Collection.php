<?php


namespace SmartOSC\Blog\Model\ResourceModel\Blog;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    protected function _construct()
    {
        $this->_init('SmartOSC\Blog\Model\Blog', 'SmartOSC\Blog\Model\ResourceModel\Blog');
        $this->_idFieldName = 'blog_id';
    }
}