<?php


namespace SmartOSC\Blog\Model\ResourceModel;


class Category extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('smartosc_categories', 'category_id');
    }
}