<?php


namespace SmartOSC\Blog\Model\ResourceModel\Category;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    protected function _construct()
    {
        $this->_init('SmartOSC\Blog\Model\Category', 'SmartOSC\Blog\Model\ResourceModel\Category');
        $this->_idFieldName = 'category_id';
    }
}