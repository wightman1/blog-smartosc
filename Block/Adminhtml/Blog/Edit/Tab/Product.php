<?php


namespace SmartOSC\Blog\Block\Adminhtml\Blog\Edit\Tab;


class Product  extends \Magento\Backend\Block\Widget\Grid\Extended
{

    protected $_coreRegistry = NULL ;

    protected $_blogFactory;

    protected $_linkFactory;

    protected $_productStatus;

    protected $_productVisibility;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\Registry $coreRegistry,
        \SmartOSC\Blog\Model\BlogFactory $blogFactory,
        \Magento\Catalog\Model\Product\LinkFactory $linkFactory,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,

        array $data = []
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_blogFactory = $blogFactory;
        $this->_linkFactory = $linkFactory;
        $this->_productStatus = $productStatus;
        $this->_productVisibility = $productVisibility;
        parent::__construct($context, $backendHelper, $data);
    }

    public function getGridUrl()
    {
        return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/*/productgrid',
            ['_current' => true]);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('blogs_products_grid');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
        if ($this->getBlog() && $this->getBlog()->getId()) {
            $this->setDefaultFilter(['in_product' => 1]);
        }
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_products') {
            $productIds = $this->_getSelectedProduct();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', ['in' => $productIds]);
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', ['nin' => $productIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    public function getBlog()
    {
        return $this->_coreRegistry->registry('blogs_blog');
    }

    protected function _prepareCollection()
    {
        $collection = $this->_linkFactory->create()->getProductCollection()
            ->addAttributeToSelect('*');
        $blogId = $this->getRequest()->getParam('blog_id');
        $blog = $this->_blogFactory->create()->load($blogId);
        if ($blog->getId() && $blog->getProductId()) {
            $productId = $blog->getProductId();
            $collection->addFieldToFilter('entity_id', $productId);
        } else {
            $associatedProductIds = [];
            $blogs = $this->_blogFactory->create()->getCollection();
            foreach ($blogs as $blog) {
                $associatedProductIds[] = $blog->getProductId();
            }

            //Get id of products that have type 'release' and haven't associated with any event
            $blogProductIds = $this->getBlogProductIds();
            foreach ($blogProductIds as $key => $id) {
                if (in_array($id, $associatedProductIds)) {
                    unset($blogProductIds[$key]);
                }
            }

            $collection->addAttributeToFilter('status', ['in' => $this->_productStatus->getVisibleStatusIds()])
                ->addFieldToFilter('entity_id', ['in' => $blogProductIds]);
            $collection->getSelect()->distinct(true)->join(
                ['stock_table' => $collection->getTable('cataloginventory_stock_status')],
                'e.entity_id = stock_table.product_id',
                []);
            $collection->getSelect()->where('stock_table.stock_status = 1');
        }
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    public function getBlogProductIds()
    {
        $blogProductIds = [];
        $productCollection = $this->_linkFactory->create()->getProductCollection()
            ->addAttributeToFilter('status', ['in' => $this->_productStatus->getVisibleStatusIds()]);
        foreach ($productCollection as $product) {
            $blogProductIds[] = $product->getId();
        }
        return $blogProductIds;
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_product',
            [
                'type' => 'checkbox',
                'html_name' => 'product_associated',
                'values' => $this->_getSelectedProduct(),
                'align' => 'center',
                'index' => 'entity_id',
                'header_css_class' => 'col-select',
                'column_css_class' => 'col-select'
            ]
        );

        $this->addColumn(
            'entity_id',
            [
                'header' => __('ID'),
                'sortable' => true,
                'type' => 'number',
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

//        $this->addColumn(
//            'product_thumbnail',
//            [
//                'header' => __('Thumbnail'),
//                'align' => 'left',
//                'width' => '97',
//                'renderer' => 'SmartOSC\Blog\Block\Adminhtml\Grid\Column\Renderer\Thumbnail'
//            ]
//        );

        $this->addColumn(
            'product_name',
            [
                'header' => __('Product Name'),
                'index' => 'name',
                'header_css_class' => 'col-name',
                'column_css_class' => 'col-name'
            ]
        );
        $this->addColumn(
            'product_sku',
            [
                'header' => __('SKU'),
                'index' => 'sku',
                'header_css_class' => 'col-sku',
                'column_css_class' => 'col-sku'
            ]
        );
        $this->addColumn(
            'product_quantity',
            [
                'header' => __('Quantity'),
                'index' => 'stock_qty',
                'header_css_class' => 'col-quantity',
                'column_css_class' => 'col-quantity'
            ]
        );
        $this->addColumn(
            'product_price',
            [
                'header' => __('Price'),
                'type' => 'currency',
                'currency_code' => (string)$this->_scopeConfig->getValue(
                    \Magento\Directory\Model\Currency::XML_PATH_CURRENCY_BASE,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ),
                'index' => 'price',
                'header_css_class' => 'col-price',
                'column_css_class' => 'col-price'
            ]
        );

        return parent::_prepareColumns();
    }

    protected function _getSelectedProduct()
    {
        $blogId = $this->getRequest()->getParam('blog_id', 0);

        $blog = $this->_blogFactory->create()->load($blogId);
        $productIdArr = [$blog->getProductId()];
        return $productIdArr;
    }
}