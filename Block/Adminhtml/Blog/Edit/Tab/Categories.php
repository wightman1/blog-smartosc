<?php


namespace SmartOSC\Blog\Block\Adminhtml\Blog\Edit\Tab;

use Magento\Backend\Block\Widget\Grid\Extended;
use phpDocumentor\Reflection\Types\This;

class Categories extends Extended
{

    protected $_coreRegistry = null;

    protected $_blogFactory;

    protected $_categoryFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \SmartOSC\Blog\Model\BlogFactory $blogFactory,
        \SmartOSC\Blog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    )
    {
        $this->_blogFactory = $blogFactory;
        $this->_categoryFactory = $categoryFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context, $backendHelper, $data);
    }

    public function getGridUrl()
    {
        return $this->_getData('grid_url') ? $this->_getData('grid_url') : $this->getUrl('*/*/categorygrid',
            ['_current' => true]);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('blogs_category_grid');
        $this->setDefaultSort('category_id');
        $this->setUseAjax(true);
        if ($this->getBlog() && $this->getBlog()->getId()) {
            $this->setDefaultFilter(['in_categories' => 1]);
        }
    }

    public function getBlog()
    {
        return $this->_coreRegistry->registry('blogs_blog');
    }

    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_categories') {
            $categoryIds = $this->_getSelectedCategories();
            if (empty($categoryIds)) {
                $categoryIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('category_id', ['in' => $categoryIds]);
            } else {
                if ($categoryIds) {
                    $this->getCollection()->addFieldToFilter('category_id', ['nin' => $categoryIds]);
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

    protected function _getSelectedCategories()
    {
        $categories = array_keys($this->getSelectedCategories());
        return $categories;
    }

    public function getSelectedCategories()
    {
        $blogId = $this->getRequest()->getParam('blog_id');
        $blog = $this->_blogFactory->create()->load($blogId);
        $categories = $blog->getCategories();

        if (!$categories) {
            return [];
        }

        $categoryIds = [];

        foreach ($categories as $categoryId) {
            $categoryIds[$categoryId] = ['id' => $categoryId];
        }

        return $categoryIds;
    }

    protected function _prepareCollection()
    {
        $collection = $this->_categoryFactory->create()->getCollection()
            ->addFieldToFilter('status', 1);
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'in_categories',
            [
                'type' => 'checkbox',
                'name' => 'in_categories',
                'values' => $this->_getSelectedCategories(),
                'align' => 'center',
                'index' => 'category_id',
                'header_css_class' => 'col-select',
                'column_css_class' => 'col-select'
            ]
        );

        $this->addColumn(
            'category_id',
            [
                'header' => __('ID'),
                'sortable' => true,
                'type' => 'number',
                'index' => 'category_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'category_name',
            [
                'header' => __('Name'),
                'type' => 'text',
                'index' => 'category_name',
                'header_css_class' => 'col-title',
                'column_css_class' => 'col-title'
            ]
        );

        return parent::_prepareColumns();
    }
}