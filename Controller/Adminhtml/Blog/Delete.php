<?php


namespace SmartOSC\Blog\Controller\Adminhtml\Blog;


class Delete  extends \Magento\Backend\App\Action
{
    protected $_pageFactory;

    protected $_blogRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \SmartOSC\Blog\Model\BlogFactory $blogFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->_blogFactory = $blogFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('blog_id');
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_blogFactory->create();
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccess(__('The blog has been deleted.'));
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['blog_id' => $id]);
            }
        }
        $this->messageManager->addError(__('We can\'t find a blog to delete.'));
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('SmartOSC_Blog::delete');
    }
}