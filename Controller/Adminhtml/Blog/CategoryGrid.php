<?php


namespace SmartOSC\Blog\Controller\Adminhtml\Blog;


class CategoryGrid extends \Magento\Backend\App\Action
{

    protected $resultLayoutFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory)
    {
        parent::__construct($context);
        $this->resultLayoutFactory = $resultLayoutFactory;
    }

    public function execute()
    {
        $resultLayout = $this->resultLayoutFactory->create();
        $resultLayout->getLayout()->getBlock('blogs.edit.tab.categorygrid')
            ->setCategories($this->getRequest()->getPost('categories', null));
        return $resultLayout;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('SmartOSC_Blogs::manage_blogs');
    }
}